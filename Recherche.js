import React, { Component } from 'react';
import { TextInput, View,Switch,Text,Button,TouchableHighlight  } from 'react-native';

export default class Recherche extends Component {
  constructor(props) {
    super(props);
    this.state = {title: '',openStations:false,banks:false};
  }

  componentDidMount() {

  }

  render() {
    const { title, openStations,banks } = this.state;

    return (
        <View style={{ flex: 1, padding: 24 }}>
          <TextInput
              placeholder="Titre"
              style={{borderBottomColor:'grey',borderBottomWidth: 2, marginBottom:20,padding:5}}
              onChangeText={(title) => this.setState({title})}
              value={title}
          />
          <View style={{flexDirection:'row', flexWrap:'wrap',justifyContent:'space-between',marginTop:20}}>
            <Text>Stations ouvertes uniquement</Text>
            <Switch
                ios_backgroundColor="#3e3e3e"
                onValueChange={(openStations) => this.setState({openStations})}
                value={openStations}
            />
          </View>
          <View style={{flexDirection:'row', flexWrap:'wrap',justifyContent:'space-between',marginTop:20}}>
            <Text>Avec Terminal de paiment uniquement</Text>
            <Switch
                ios_backgroundColor="#3e3e3e"
                onValueChange={(banks) => this.setState({banks})}
                value={banks}
            />
          </View>
          <TouchableHighlight style={{backgroundColor: '#D6D8D7',marginTop:50,padding:10}}>
            <Button color='black' title="Filtrer" onPress={() => this.props.navigation.navigate('BiclooFinder',{filtres:title,openStations : openStations,banks:banks})}/>
          </TouchableHighlight>
        </View>
    );
  }
};