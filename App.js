import React, { Component } from 'react';
import { Button } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import { NavigationContainer  } from '@react-navigation/native';
import { createNativeStackNavigator} from '@react-navigation/native-stack';
import List from './List';
import DetailsStation from './DetailsStation';
import Recherche from './Recherche';
import {StyleSheet} from "react-native";

export default class App extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        const Stack = createNativeStackNavigator();
        const styles = StyleSheet.create({
            header : {
                backgroundColor : "#3F51B5",
            }
        });

      return (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="BiclooFinder" component={List} options={({ navigation }) => ({headerStyle:styles.header,headerTintColor:'#fff', headerRight: () => ( <Ionicons name="search" size={32} color="white" onPress={() => navigation.navigate('Recherche')} />),})} />
            <Stack.Screen name="Details" component={DetailsStation} options={{headerStyle:styles.header,headerTintColor:'#fff',headerBackTitle:'Retour'}} />
            <Stack.Screen name="Recherche" component={Recherche} options={{headerStyle:styles.header,headerTintColor:'#fff',headerBackTitle:'Retour'}} />
          </Stack.Navigator>
        </NavigationContainer>
    );
  }
};

