import React, { Component } from 'react';
import { ActivityIndicator, FlatList, Text, View,TouchableWithoutFeedback,StyleSheet } from 'react-native';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
import moment from 'moment';
import Ionicons from "@expo/vector-icons/Ionicons";
moment.locale('fr')

export default class DetailsStation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      station: {},
    };
  }

  componentDidMount() {
    this.props.navigation.setOptions({title:this.props.route.params.item.name});
    this.setState({ station: this.props.route.params.item });
  }

  render() {
    const { station } = this.state;

    const styles = StyleSheet.create({
      row : {
        flexDirection:'row',
        flexWrap:'wrap'
      },
      rowStation : {
        justifyContent:'space-evenly',
        marginBottom: 15,
        marginTop: 10,
      },
      textForAvailable : {
        fontSize: 20,
      },
      textGreen : {
        color:'green'
      },
      textOrange : {
        color:'orange'
      },
      textRed : {
        color:'red'
      }
    });

    return (
        <View style={{ flex: 1, padding: 24 }}>
            <Text>Dernière mise à jour le {moment(station.last_update).format("DD/MM/YYYY, HH:mm:ss")}</Text>
            {station.status === 'OPEN' ? <Text style={styles.textGreen}>Station ouverte</Text> : <Text style={styles.textRed}>Station fermée</Text>}
            {station.banking === true ? <Text style={styles.textGreen}>Terminal de paiement disponible</Text> : <Text style={styles.textRed}>Terminal de paiement non disponible</Text>}
          <View style={[styles.row,styles.rowStation]}>
              <Text style={[styles.textForAvailable,station.available_bikes > 5 ? styles.textGreen : styles.textOrange]}>{station.available_bikes} vélos</Text>
            <Text style={[styles.textForAvailable,station.available_bike_stands > 5 ? styles.textGreen : styles.textOrange]}>{station.available_bike_stands} places</Text>
          </View>
          <Text>{station.address}</Text>
          <MapView
              style={{flex:1}}
              initialRegion={{
                latitude: this.props.route.params.item.position.lat,
                longitude: this.props.route.params.item.position.lng,
                latitudeDelta: 0.02,
                longitudeDelta: 0.02,
              }}
          >
            <Marker
                key={1}
                coordinate={{ latitude :this.props.route.params.item.position.lat , longitude :this.props.route.params.item.position.lng}}
                title={this.props.route.params.item.name}
            />
          </MapView>
        </View>
    );
  }
};