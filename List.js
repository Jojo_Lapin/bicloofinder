import React, { Component } from 'react';
import { ActivityIndicator, FlatList, Text, View,TouchableWithoutFeedback,StyleSheet } from 'react-native';
import moment from 'moment';
moment.locale('fr')

export default class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true
    };
  }

  async getStations() {
    try {
      const response = await fetch('https://api.jcdecaux.com/vls/v1/stations?contract=Nantes&apiKey=cbaae4edd2a305b6b8bfdbfbd87610896d5ab354');
      const json = await response.json();
      this.setState({ data: json });
    } catch (error) {
      console.log(error);
    } finally {
      this.setState({ isLoading: false });
    }
  }

  componentDidMount() {
    this.getStations();
  }

  render() {
    const { data, isLoading } = this.state;

    const styles = StyleSheet.create({
      container: {
        marginTop: 10,
        borderBottomColor: 'gray',
        borderBottomWidth: 2,
      },
      nameStation: {
        fontWeight: 'bold',
        fontSize: 20,
      },
      addressStation: {
        fontSize: 18,
        marginBottom: 5,
      },
      row : {
        flexDirection:'row',
        flexWrap:'wrap'
      },
      rowStation : {
        justifyContent:'space-evenly',
        marginBottom: 10,
        marginTop: 5,
      },
      textForAvailable : {
        fontSize: 20,
      },
      textGreen : {
        color:'green'
      },
      textOrange : {
        color:'orange'
      }
    });

    return (
        <View style={{ flex: 1, padding: 24 }}>
          {isLoading ? <ActivityIndicator/> : (
              <FlatList
                  data={data}
                  keyExtractor={item => item.name}
                  renderItem={({ item }) => (
                      <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Details',{item:item})}>
                        <View style={styles.container}>
                          <Text>Dernière mise à jour le {moment(item.last_update).format("DD/MM/YYYY, HH:mm:ss")}</Text>
                          <Text style={styles.nameStation}>{item.name}</Text>
                          <Text style={styles.addressStation}>{item.address}</Text>
                          <View style={[styles.row,styles.rowStation]}>
                            <Text style={[styles.textForAvailable,item.available_bikes > 5 ? styles.textGreen : styles.textOrange]}>{item.available_bikes} vélos</Text>
                            <Text style={[styles.textForAvailable,item.available_bike_stands > 5 ? styles.textGreen : styles.textOrange]}>{item.available_bike_stands} places</Text>
                          </View>
                        </View>
                      </TouchableWithoutFeedback>
                  )}
              />
          )}
        </View>
    );
  }


};